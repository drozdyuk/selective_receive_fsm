-module(fsm).
-behaviour(gen_statem).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FSM machine that when started,
%% waits for the "done" command.
%% Either blocks (if it's a call)
%% or eats all the messages before 
%% that.
-export([done/0, init/1, shout/1, howareyou/0,
	callback_mode/0, handle_event/4, code_change/4,
	terminate/3, start_link/0]).
start_link() ->
	gen_statem:start_link({local,?MODULE}, ?MODULE, [], []).

init([]) ->
	erlang:send_after(5000, self(), unlock),
	{ok, doing, #{}}.

% Call
howareyou() ->
	gen_statem:call(?MODULE, howareyou).
% Cast
shout(Msg) ->
	gen_statem:cast(?MODULE, {shout, Msg}).

done() ->
	gen_statem:call(?MODULE, done).

callback_mode() -> 
	handle_event_function.

%% Doing state - stash messages
handle_event({call, From}, done, doing, Data) ->
	{next_state, done, Data, [{reply, From, ok}]};

handle_event(info, unlock, doing, Data) ->
	io:format("GOT UNLOCK"),
	{next_state, done, Data};

handle_event(T, Msg, doing, _Data) ->
	io:format("Stashing [~p]:...: ~p~n", [T,Msg]),
	{keep_state_and_data, [postpone]};	

%% Done state - handle things!
handle_event({call, From}, howareyou, done, _Data) ->
	io:format("Replying to howareyou...~n"),
	{keep_state_and_data, [{reply, From, i_am_ok}]};

handle_event(cast, {shout, Msg}, done, _Data) ->
	io:format("Got your shout: ~p~n", [Msg]),
	keep_state_and_data.

terminate(_, _, _) -> ok.
code_change(_Vsn, State, Data, _Extra) ->
	{ok,State,Data}.

